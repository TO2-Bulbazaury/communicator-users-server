package communicator.users.server;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.core.util.StringCollectionUtil;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jakub Janusz on 2015-12-09.
 */
public class ClientConnector {

    private int socket;
    private ServerSocket serverSocket;
    private List<Socket> clients;
    private DatabaseConnector databaseConnector;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private List<String> usersOnline;

    public ClientConnector(int socket) {
        this.socket = socket;
        this.databaseConnector = new DatabaseConnector();
        this.usersOnline = new LinkedList<>();
    }

    public void listenSocket() throws IOException {
        serverSocket = new ServerSocket(socket);
        clients = new LinkedList<>();

        while(true) {
            Socket client;
            client = serverSocket.accept();
            clients.add(client);

            logger.info("Incoming connection from {}.", client);

            new RequestsHandler(client, databaseConnector, usersOnline).start();
        }
    }

}
