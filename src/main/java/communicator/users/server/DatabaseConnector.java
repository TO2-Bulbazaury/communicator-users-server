package communicator.users.server;

import ch.qos.logback.classic.Logger;
import communicator.users.server.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.LoggerFactory;

import java.util.List;

import static communicator.users.server.persistence.HibernateUtils.session;

/**
 * Created by Jakub Janusz on 2015-12-09.
 */

class AuthorizeException extends Exception {

}

public class DatabaseConnector {

    private String SEPARATOR = "#$#";
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public DatabaseConnector() {

    }

    public boolean authorize(User user) {
        try {
            Session session = session();
            //Transaction transaction = session.getTransaction();

            List<Object[]> result = session.createQuery("select u from User u where u.username like :username and u.password like :password")
                    .setParameter("username", user.getUsername())
                    .setParameter("password", user.getPassword())
                    .list();

            //transaction.commit();
            session.close();

            if(result.size() != 1)
                throw new AuthorizeException();
            else
                return true;

        } catch (Exception e) {
            logger.error("Authorize error:", e);
            return false;
        }
    }

    public boolean register(User user) {
        boolean exceptionOccurred = false;
        boolean isEverythingOK;
        try {
            Session session = session();
            Transaction transaction = session.beginTransaction();

            session.persist(user);

            transaction.commit();
            session.close();
        } catch (Exception e) {
            exceptionOccurred = true;
            logger.error(e.toString());
        } finally {
            isEverythingOK = !exceptionOccurred;
        }
        return isEverythingOK;
    }

    public String getProfileData(String username) {
        Session session = session();

        List<User> result = session.createQuery("select u from User u where u.username like :username").
                setParameter("username", username).
                list();

        for (User o : result)
            System.out.println(o.toString());

        session.close();

        String response = "";

        return response;

    }

}
