package communicator.users.server;

import ch.qos.logback.classic.Logger;
import communicator.users.server.model.User;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Random;

/**
 * Created by Jakub Janusz on 2015-12-09.
 */
public class RequestsHandler extends Thread {

    private final String SEPARATOR = "#$#";
    private Socket client;
    private DatabaseConnector databaseConnector;
    private BufferedReader reader;
    private PrintWriter writer;
    private User user;
    private String token;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private List<String> usersOnline;

    public RequestsHandler(Socket client, DatabaseConnector databaseConnector, List<String> usersOnline) throws IOException {
        this.client = client;
        this.databaseConnector = databaseConnector;
        this.reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.writer = new PrintWriter(client.getOutputStream(), true);
        this.user = new User();
        this.token = "0";
        this.usersOnline = usersOnline;
    }

    public void run() {
        while(true) {
            String request;
            try {
                request = reader.readLine();
                logger.info("Received following request from {}:\n\t{}", client, request);
                String response = parseRequest(request);
                logger.info("Sent following response to {}:\n\t{}", client, response);
                writer.println(response);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String parseRequest(String request) {
        /* TODO
        * autoryzacja - A
        * rejestracja - R
        * usuwanie konta - D
        * pobranie aktywnych u�ytkownikow - U
        * pobranie danych profilowych - P
        * edycja danych profilowych - E
        * dezaktywacja konta - X
        * aktwacja konta - Y
        * ban - B
        * */
        String response = null;
        String[] parts = request.split("[#][$][#]");
        String type = parts[0];
        switch (type) {
            case "A":
                if (parts.length != 3) {
                    logger.warn("Incorrect number of arguments for request of A type.");
                } else {
                    this.user.setUsername(parts[1]);
                    this.user.setPassword(parts[2]);
                    response = "A" + SEPARATOR;
                    logger.info(databaseConnector.toString());
                    if(databaseConnector.authorize(this.user)) {
                        this.token = generateToken();
                        response += this.token;
                        this.usersOnline.add(this.user.getUsername());
                    } else {
                        this.user = new User();
                        response += "0";
                    }
                }
                break;
            case "R":
                if (parts.length != 8) {
                    logger.warn("Incorrect number of arguments for request of R type.");
                } else if(!parts[1].equals(this.token)) {
                    logger.warn("Invalid session token.");
                    response = "R" + SEPARATOR + "rej";
                } else {
                    this.user.setUsername(parts[2]);
                    this.user.setPassword(parts[3]);
                    this.user.setEmail(parts[4]);
                    this.user.setName(parts[5]);
                    this.user.setAge(parts[6]);
                    this.user.setCity(parts[7]);
                    response = "R" + SEPARATOR;
                    if(databaseConnector.register(user)) {
                        response += "acc";
                    } else {
                        this.user = new User();
                        response += "rej";
                    }
                }
                break;
            case "U":
                if(parts.length != 3)
                    logger.warn("Incorrect number of arguments for request of U type.");
                else if(!parts[1].equals(this.token)) {
                    logger.warn("Invalid session token.");
                    response = "U";
                } else {
                    response = "U";
                    for (String user : usersOnline) {
                        response += SEPARATOR;
                        response += user;
                    }
                }
                break;
            case "P":
                if(parts.length != 3)
                    logger.warn("Incorrect number of arguments for request of p type.");
                else if(!parts[1].equals(this.token)) {
                    logger.warn("invalid session token.");
                    response = "P";
                } else {
                    response = "P";
                    response += databaseConnector.getProfileData(parts[2]);
                }
                break;
            default:
                logger.warn("parseRequest - Request type does not match any of possible types.");
                break;
        }

        return response;
    }

    private String generateToken() {
        Random random = new Random();
        int token = random.nextInt(900) + 100;
        return Integer.toString(token);
    }

}
