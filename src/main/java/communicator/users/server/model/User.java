package communicator.users.server.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Dawid on 2015-12-05.
 */
public class User {

    private long id;
    private String username;
    private String password;
    private String email;
    private String name;
    private String age;
    private String city;

    public User() {
    }

    private long getId() {
        return this.id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
