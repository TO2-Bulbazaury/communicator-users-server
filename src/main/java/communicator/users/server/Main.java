package communicator.users.server;

import communicator.users.server.persistence.HibernateUtils;

import java.io.IOException;

/**
 * Created by Jakub Janusz on 2015-12-09.
 */
public class Main {

    private static int socket;
    private static ClientConnector clientConnector;

    public static void main(String[] args) {
        HibernateUtils.session().close();

        socket = 3333;
        clientConnector = new ClientConnector(socket);

        try {
            clientConnector.listenSocket();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

}
